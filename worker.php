<?php
$dir = "/var/www/livess/";
//include($dir . "/ipdata/geoipcity.inc");
//include($dir . "/ipdata/geoipregionvars.php");
$config['useragent'] = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.6) Gecko/20100625 Firefox/3.6.6";
$config['cookie_file'] = '/var/www/cookie/1.txt';
if(!file_exists($config['cookie_file'])){
    $fp = @fopen($config['cookie_file'],'w');
    @fclose($fp);
}
function str_contains($haystack, $needle, $ignoreCase = false) {
    if ($ignoreCase) {
        $haystack = strtolower($haystack);
        $needle   = strtolower($needle);
    }
    $needlePos = strpos($haystack, $needle);
    return ($needlePos === false ? false : ($needlePos+1));
}
function array_remove_empty($arr){
    $narr = array();
    while(list($key, $val) = each($arr)){
        if (is_array($val)){
            $val = array_remove_empty($val);
            // does the result array contain anything?
            if (count($val)!=0){
                // yes :-)
                $narr[$key] = trim($val);
            }
        }
        else {
            if (trim($val) != ""){
                $narr[$key] = trim($val);
            }
        }
    }
    unset($arr);
    return $narr;
}
function curl($url, $var=false,	$sock=false)
{
	global $config;
	$ch = curl_init(); 
	// Initialize a CURL session.       
	curl_setopt($ch, CURLOPT_URL, $url);  // The URL to fetch. You can also set this when initializing a session with curl_init().  
	curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']); // The contents of the "User-Agent: " header to be used in a HTTP request.  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $config['cookie_file']);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $config['cookie_file']);
	curl_setopt($ch, CURLOPT_TIMEOUT,$config['timeout']);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
    if ($sock) {
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		curl_setopt($ch, CURLOPT_PROXY, $sock);
		curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    }
	if($var){
		$post_array = array();
		if(!is_array($var))
		{
			return false;
		}
		foreach($var as $key => $value)
		{
			$post_array[] = rawurlencode($key) . "=" . rawurlencode($value);
		}
		$post_string = implode("&", $post_array);	
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	}
	$result = curl_exec($ch);  // grab URL and pass it to the variable.  
	curl_close($ch);  // close curl resource, and free up system resources.   
	return $result; // Print page contents. 
}
function delete_cookies(){
	global $config;
    $fp = @fopen($config['cookie_file'],'w');
	fwrite($fp, "");
    @fclose($fp);	
}
function get($list)
{
	preg_match_all("/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,7}/", $list, $socks);
	return $socks;
}

	function check($socks)
	{
		global $config,$dir,$_POST;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $config['cookie_file']);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $config['cookie_file']);
		curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
		curl_setopt($curl, CURLOPT_PROXY, $socks);
		curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		curl_setopt($curl, CURLOPT_TIMEOUT,3);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT,0);
		curl_setopt($curl, CURLOPT_USERAGENT, $config['useragent']);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_URL, "http://www.google.com");
		$Exec = curl_exec($curl);
		
		if ($Exec)
		{
			$info = curl_getinfo($curl);
			$Times = $info['connect_time'];
			$status = "Live";
		}
		else{
			$Times = 0;
			$status = curl_error($curl);
		}	
		curl_close ($curl);
		$Result = '';
		$Result .= $status;
		$Result .= " | ".$socks."\n";
		return $Result;
	}
while(true){
$rad_num = $argv[1];
$lines = file($dir.'socks'.$rad_num.'.txt');
if(count($lines)>0){
$first_line = $lines[0];
$lines[0] = ""; 
$fp0 = fopen($dir.'socks'.$rad_num.'.txt', 'w'); 
fwrite($fp0, implode($lines));
fclose($fp0);
if(str_contains($first_line, ".*")) {
	$mode = "scan";
	$s1 = "";
	for($i2 = 1; $i2 < 255; $i2++){
       $s1 .= str_replace("***",$i2,$first_line)."\n";
    }
    $s = get($s1);
} else {
	$s = get($first_line);
}
	$allSocks = array_remove_empty(array_unique($s[0]));
	$all = count($allSocks);
	$live = array();
	foreach($allSocks AS $k => $socksx){
		$checked = check($socksx);
		// if(str_contains($checked,'Live')) {
		// 	$path=$dir."live.txt";
		// } else {
		// 	$path=$dir."die.txt";
		// }
		// 		$files1=fopen($path, "a");
		// 		fwrite($files1,$checked);
		// 		fclose($files1);
		if(str_contains($checked,'Live')) {
			$path=$dir."live.txt";
			$files1=fopen($path, "a");
			fwrite($files1,$checked);
			fclose($files1);
		}
				
			if(str_contains($checked,'Live') && $mode == "scan") {
				break;
			}
	}
}
sleep(rand(0,3));
}
?>