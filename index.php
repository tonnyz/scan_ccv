<?php
error_reporting(0);
$dir = dirname(__FILE__);
include($dir . "/ipdata/geoipcity.inc");
include($dir . "/ipdata/geoipregionvars.php");
$config['useragent'] = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.6) Gecko/20100625 Firefox/3.6.6";
$config['cookie_file'] = $dir . '/cookie/'. $_SERVER['REMOTE_ADDR'] . '.txt';
if(!file_exists($config['cookie_file'])){
    $fp = @fopen($config['cookie_file'],'w');
    @fclose($fp);
}
function array_remove_empty($arr){
    $narr = array();
    while(list($key, $val) = each($arr)){
        if (is_array($val)){
            $val = array_remove_empty($val);
            // does the result array contain anything?
            if (count($val)!=0){
                // yes :-)
                $narr[$key] = trim($val);
            }
        }
        else {
            if (trim($val) != ""){
                $narr[$key] = trim($val);
            }
        }
    }
    unset($arr);
    return $narr;
}
function curl($url, $var=false,	$sock=false)
{
	global $config;
	$ch = curl_init(); 
	// Initialize a CURL session.       
	curl_setopt($ch, CURLOPT_URL, $url);  // The URL to fetch. You can also set this when initializing a session with curl_init().  
	curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']); // The contents of the "User-Agent: " header to be used in a HTTP request.  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $config['cookie_file']);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $config['cookie_file']);
	curl_setopt($ch, CURLOPT_TIMEOUT,$config['timeout']);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
    if ($sock) {
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		curl_setopt($ch, CURLOPT_PROXY, $sock);
		curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    }
	if($var){
		$post_array = array();
		if(!is_array($var))
		{
			return false;
		}
		foreach($var as $key => $value)
		{
			$post_array[] = rawurlencode($key) . "=" . rawurlencode($value);
		}
		$post_string = implode("&", $post_array);	
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	}
	$result = curl_exec($ch);  // grab URL and pass it to the variable.  
	curl_close($ch);  // close curl resource, and free up system resources.   
	return $result; // Print page contents. 
}
function delete_cookies(){
	global $config;
    $fp = @fopen($config['cookie_file'],'w');
	fwrite($fp, "");
    @fclose($fp);	
}
function get($list)
{
	preg_match_all("/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,7}/", $list, $socks);
	return $socks;
}
	set_time_limit(3600);
	


	function check($socks)
	{
		global $config,$dir,$_POST;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $config['cookie_file']);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $config['cookie_file']);
		curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
		curl_setopt($curl, CURLOPT_PROXY, $socks);
		curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		curl_setopt($curl, CURLOPT_TIMEOUT,3);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT,0);
		curl_setopt($curl, CURLOPT_USERAGENT, $config['useragent']);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_URL, "http://www.google.com");
		$Exec = curl_exec($curl);
		
		if ($Exec)
		{
			$info = curl_getinfo($curl);
			$Times = $info['connect_time'];
			$status = "<font color='yellow'><b>Live</b></font>";
		}
		else{
			$Times = 0;
			$status = "<font color='red'><b>" . curl_error($curl) . "</b></font>";
		}	
		curl_close ($curl);
		$IP = explode(":",$socks);
		$IP = $IP[0];
		$IP = str_replace(",", ".", $IP);
		$gi = geoip_open($dir . "/ipdata/GeoLiteCity.dat",GEOIP_STANDARD);
		$record = geoip_record_by_addr($gi, $IP);
		$country_code = $record->country_code;
		$country_name = $record->country_name;
		$region = $record->region;
		$city = $record->city;
		$postal_code = $record->postal_code;
		$area_code = $record->area_code;
		geoip_close($gi);
		if (empty($country_code)) $country_code = "Unknown";
		if (empty($country_name)) $country_name = "Unknown";
		if (empty($region)) $region = "Unknown";
		if (empty($city)) $city = "Unknown";
		if (empty($postal_code)) $postal_code = "Unknown";
		if (empty($area_code)) $area_code = "Unknown";
		$Result = '';
		$Result .= $status;
		$Result .= " | <font color='white'>".$socks."</font>";
		if ($_POST["Times"]) $Result .= " | Times: <font color='#00ff00'>".$Times."</font>";
		if ($_POST["City"]) $Result .= " | City: <font color='orange'>".$city."</font>";
		if ($_POST["State"]) $Result .= " | State: <font color='#ff00ff'>".$region."</font>";
		if ($_POST["Zipcode"]) $Result .= " | Zipcode: <font color='yellow'>".$postal_code."</font>";
		if ($_POST["Areacode"]) $Result .= " | Areacode: <font color='#00ffff'>".$area_code."</font>";
		if ($_POST["Country"]) $Result .= " | Country: <b>".$country_name."</b>";
		if($_POST['Paypal'] AND $Exec){
			$var = array(
				'id' => 13185,
				'amount' => 20,
				'submit' => 'Donate!'
			);
			delete_cookies();
			$config['timeout'] = $_POST['timeout'];
			$str = curl("http://www.dreamhost.com/donate.cgi",$var,$socks);
			if($str){
				if(stripos($str,'MF:1X:Button:Entry:Billing')!==false) 
				$Result .= " | Paypal: <font color='green'><b>IP Clear</b></font> ";
				else $Result .= " | Paypal: <font color='red'><b>Blacklist</b></font> ";
			}
			else $Result .= " | Paypal: <font color='white'>Timeout</font> ";
		}
		return $Result;
	}
$socks_list = $_POST["list_socks"];
$s = get($socks_list);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
	<title>...:: Check Socks 5 Live & Blacklist Paypal ::...</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		body
		{
			background-color: #333333;
			font-size: 12pt;
		}
		body,td,th {
			color: #99CC00;
		}
		h2
		{
			color: #FFCC00;
		}
	</style>
		
			 <script>
function fucksock(){
	var sockscave = window.document.checkproxy.list_socks.value;
	var fuck = sockscave.match(/\d{1,3}([.])\d{1,3}([.])\d{1,3}([.])\d{1,3}((:)|(\s)+)\d{1,8}/g );
	if(fuck){
		var list="";
		for(var i=0;i<fuck.length;i++){
			if(fuck[i].match(/\d{1,3}([.])\d{1,3}([.])\d{1,3}([.])\d{1,3}(\s)+\d{1,8}/g )){
				fuck[i]=fuck[i].replace(/(\s)+/,':');
			}
			list=list+fuck[i];
			if(i!=(fuck.length-1)){
				list=list+"\n";
			}
		}
		window.document.checkproxy.list_socks.value=list;
	}
	else{
		window.document.checkproxy.list_socks.value="";
	}
}
</script>	
</head>
<body>
	<center><h2>Check Socks Online </h2></center>

<form name="checkproxy" method="post" action="index.php">
<center><textarea name="list_socks" cols="122" rows="10"><?php if($_POST['btn-submit']) echo implode("\n",$s[0]);?></textarea></center>
<div align='center'>
<input name="Id" type="checkbox" id="Id" checked="checked" /><label for="Id">Id</label>&nbsp;
<input name="Times" type="checkbox" id="Times" checked="checked" /><label for="Times">Times</label>&nbsp;
<input name="City" type="checkbox" id="City" checked="checked" /><label for="City">City</label>&nbsp;
<input name="State" type="checkbox" id="State" checked="checked" /><label for="State">State</label>&nbsp;
<input name="Zipcode" type="checkbox" id="Zipcode" checked="checked" /><label for="Zipcode">Zipcode</label>&nbsp;
<input name="Areacode" type="checkbox" id="Areacode" checked="checked" /><label for="Areacode">Areacode</label>&nbsp;
<input name="Country" type="checkbox" id="Country" checked="checked" /><label for="Country">Country</label>&nbsp;
<input name="Paypal" type="checkbox" id="Paypal" /><label for="Paypal"><font color=green><b>CLEAR PAYPAL</b></font></label> &nbsp;
<input name="timeout" type="input" size="3" value="5" />&nbsp;
<input value="L&#7885;c socks" onclick="fucksock();return false;" type="button"> <input type="submit" value=" B&#7855;t &#273;&#7847;u " name="btn-submit" /><br></center>

</div>
</form>

<?php
function vbflush()
{
	static $output_handler = null;
	if ($output_handler === null)
	{
		$output_handler = @ini_get('output_handler');
	}

	if ($output_handler == 'ob_gzhandler')
	{
		// forcing a flush with this is very bad
		return;
	}

	flush();
	if (function_exists('ob_flush') AND function_exists('ob_get_length') AND ob_get_length() !== false)
	{
		@ob_flush();
	}
	else if (function_exists('ob_end_flush') AND function_exists('ob_start') AND function_exists('ob_get_length') AND ob_get_length() !== FALSE)
	{
		@ob_end_flush();
		@ob_start();
	}
}
if($_POST["btn-submit"]){
	$allSocks = array_remove_empty(array_unique($s[0]));
	$all = count($allSocks);
	$live = array();
	$i=0;
	echo "<b><i><center>...:: Checking Socks with timeout 3s ::...</center></i></b><HR>";
	foreach($allSocks AS $k => $socksx){
		$checked = check($socksx);
		$pro = (($_POST['Id']) ? (($i<9) ? "0".($i+1) : ($i+1))." - " : "") . $checked."<br />";
		if(stripos($checked,'live')==24) $live[] = $pro;
		echo $pro;
		$i++;
		vbflush();
	}
	echo "<HR><center>Total: <font color='white'>$all</font> Socks ||  Live <font color='green'>" . count($live) . "</font> ||  Die <font color='red'>" . ($all - count($live)) . "</font> <br></center><HR><font color='green'><b><center>...:: List Socks live ::...</center></b></font><HR>";
	echo implode("\n",$live);
}
?>

</body>
</html>